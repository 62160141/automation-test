import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://myid.buu.ac.th/')

WebUI.click(findTestObject('Object Repository/Page_My ID/a_Change passwordRenew password (2)'))

WebUI.click(findTestObject('Object Repository/Page_My ID/form_(Username)                            _e764cf'))

WebUI.setText(findTestObject('Object Repository/Page_My ID/input_(Username)_user (4)'), '62160141')

WebUI.click(findTestObject('Object Repository/Page_My ID/button_Enter'))

WebUI.setEncryptedText(findTestObject('Object Repository/Page_My ID/input_(Old Password)_oldpass'), 'lGZg/gvHBV2Cj9rQgotueA==')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_My ID/input_(New Password)_newpass (2)'), 'IaMsv0sjfA09QmCCU/doiwC/9v2MT/eoO+gnHqcYsmQ=')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_My ID/input_(Re-New Password)_renewpass (2)'), 'pvJsclKw296dcV+u58q5CUZfpnnauCNJ+pwEakZdRmE=')

WebUI.click(findTestObject('Object Repository/Page_My ID/button_Change Password (2)'))

WebUI.click(findTestObject('Object Repository/Page_My ID/div_Style the container for inputs .contain_85f58a'))

WebUI.closeBrowser()

